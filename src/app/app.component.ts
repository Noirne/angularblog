import { Component, OnInit } from '@angular/core';
import { PostListComponent } from './PostListComponent/post-list-item.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  posts: any[];

  constructor(private PostListItem: PostListComponent){

  }

  ngOnInit(){
    this.posts = this.PostListItem.posts;
  }
}
