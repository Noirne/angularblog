export class PostListComponent {
    posts = [
      {
        postListTitle: "My first post",
        postListContent: "Lorem ipsum"
      },
      {
        postListTitle: "My second post",
        postListContent: "Another Lorem ipsum"
      },
      {
        postListTitle: "third post",
        postListContent: "A final Lorem ipsum"
      }
    ];
  }  