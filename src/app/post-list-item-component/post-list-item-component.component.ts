import { Component, Input, OnInit } from '@angular/core';
import { PostListComponent } from '../PostListComponent/post-list-item.component';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})
export class PostListItemComponentComponent implements OnInit {

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() loveIts: number;
  @Input() created_at;

  constructor(private postListComponent: PostListComponent) {
    this.loveIts = 0;
   }

  ngOnInit() {
     this.created_at = new Date();//new Promise((resolve, reject) => {
    //     const date = new Date();
    //     setTimeout(
    //       () => {
    //         resolve(date);
    //       }, 2000
    //     );
    // });
  }

  onLoveClicked() { this.loveIts++;}
  onDislikeClicked() { this.loveIts--;}


}
